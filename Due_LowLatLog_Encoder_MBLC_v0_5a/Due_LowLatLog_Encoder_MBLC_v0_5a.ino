/**
 * This program logs data to a binary file.  Functions are included
 * to convert the binary file to a csv text file.
 *
 * Samples are logged at regular intervals.  The maximum logging rate
 * depends on the quality of your SD card and the time required to
 * read sensor data.  This example has been tested at 500 Hz with
 * good SD card on an Uno.  4000 HZ is possible on a Due.
 *
 * If your SD card has a long write latency, it may be necessary to use
 * slower sample rates.  Using a Mega Arduino helps overcome latency
 * problems since 12 512 byte buffers will be used.
 *
 * Data is written to the file using a SD multiple block write command.
 */
//Works fine with collected simulated MBLC data from teensy with
//no more than 196 usec max latency between writes! -v0_1
//v0_2 includes renaming file according to header
// -   simplifies indexing file to convert and dump
//v03 confirmed to work with Boom & MBLC!
//v04 implement deleting a file/wiping out entire SD
//v05 reduce Serial output. Enable binary data dump over serial in 128 byte writes.
//v05a further flexiblitiy of binary dump function

//-----------------------SD handling libraries
#include <SPI.h>
#include "SdFat.h"
#include "FreeStack.h"
#include "UserTypes.h"

#ifdef __AVR_ATmega328P__
#include "MinimumSerial.h"
MinimumSerial MinSerial;
#define Serial MinSerial
#endif // __AVR_ATmega328P__

HardwareSerial &SER = Serial3; //USB - Serial || WIFI Serial3
HardwareSerial &DEB = Serial;  //Serial1; //Debug with teensy - change on usrFun.cpp
bool printDEB = true;
//==============================================================================
// Start of configuration constants.
//==============================================================================
// Abort run on an overrun.  Data before the overrun will be saved.
#define ABORT_ON_OVERRUN 1

//------------------------------------------------------------------------------
// Set USE_SHARED_SPI non-zero for use of an SPI sensor.
// May not work for some cards.
#ifndef USE_SHARED_SPI
#define USE_SHARED_SPI 0 //CHANGE IF NOT USING SHARED SPI !!!!
#endif                   // USE_SHARED_SPI
//------------------------------------------------------------------------------
// Pin definitions.
//
// SD chip select pin.
const uint8_t SD_CS_PIN = 10; //DUE w Eth ctrlr - 4, DUE w/o Eth ctrlr - 10 , teensy - SS;
//
// Digital pin to indicate an error, set to -1 if not used.
// The led blinks for fatal errors. The led goes on solid for
// overrun errors and logging continues unless ABORT_ON_OVERRUN
// is non-zero.
#ifdef ERROR_LED_PIN
#undef ERROR_LED_PIN
#endif // ERROR_LED_PIN
const int8_t ERROR_LED_PIN = LED_BUILTIN;
//------------------------------------------------------------------------------
// File definitions.
//
// Maximum file size in blocks.
// The program creates a contiguous file with FILE_BLOCK_COUNT 512 byte blocks.
// This file is flash erased using special SD commands.  The file will be
// truncated if logging is stopped early.
const uint32_t FILE_BLOCK_COUNT = 256000;
//----------------------------------------------------------------------*********
// log file base name if not defined in UserTypes.h
#ifndef FILE_BASE_NAME
#define FILE_BASE_NAME "data"
#endif // FILE_BASE_NAME
//----------------------------------------------------------------------*********
// Buffer definitions.
//
// The logger will use SdFat's buffer plus BUFFER_BLOCK_COUNT-1 additional
// buffers.
//
#ifndef RAMEND
// Assume ARM. Use total of ten 512 byte buffers.
const uint8_t BUFFER_BLOCK_COUNT = 10;
//
#elif RAMEND < 0X8FF
#error Too little SRAM
//
#elif RAMEND < 0X10FF
// Use total of two 512 byte buffers.
const uint8_t BUFFER_BLOCK_COUNT = 2;
//
#elif RAMEND < 0X20FF
// Use total of four 512 byte buffers.
const uint8_t BUFFER_BLOCK_COUNT = 4;
//
#else  // RAMEND
// Use total of 12 512 byte buffers.
const uint8_t BUFFER_BLOCK_COUNT = 12;
#endif // RAMEND
//==============================================================================
// End of configuration constants.
//==============================================================================
// Temporary log file.  Will be deleted if a reset or power failure occurs.
#define TMP_FILE_NAME FILE_BASE_NAME "##.BIN"

// Size of file base name.----------------------------------***********
const uint8_t BASE_NAME_SIZE = sizeof(FILE_BASE_NAME) - 1;
const uint8_t FILE_NAME_DIM = 100; //BASE_NAME_SIZE + 7;
char binName[FILE_NAME_DIM] = FILE_BASE_NAME "00.bin";

bool CUSTOM_NAME = false;

SdFat sd;

SdBaseFile binFile;

// Number of data records in a block.
const uint16_t DATA_DIM = (512 - 4) / sizeof(data_t);

//Compute fill so block size is 512 bytes.  FILL_DIM may be zero.
const uint16_t FILL_DIM = 512 - 4 - DATA_DIM * sizeof(data_t);

struct block_t
{
  uint16_t count;
  uint16_t overrun;
  data_t data[DATA_DIM];
  uint8_t fill[FILL_DIM];
};
//==============================================================================
// Error messages stored in flash.
#define error(msg)               \
  {                              \
    sd.errorPrint(&SER, F(msg)); \
    fatalBlink();                \
  }
//------------------------------------------------------------------------------
//
void fatalBlink()
{
  while (true)
  {
    SysCall::yield();
    if (ERROR_LED_PIN >= 0)
    {
      digitalWrite(ERROR_LED_PIN, HIGH);
      delay(200);
      digitalWrite(ERROR_LED_PIN, LOW);
      delay(200);
    }
  }
}
//------------------------------------------------------------------------------
// read data file and check for overruns
void checkOverrun()
{
  bool headerPrinted = false;
  block_t block;
  uint32_t bn = 0;

  if (!binFile.isOpen())
  {
    SER.println();
    SER.println(F("No current binary file"));
    return;
  }
  binFile.rewind();
  SER.println();
  SER.print(F("FreeStack: "));
  SER.println(FreeStack());
  SER.println(F("Checking overrun errors - type any character to stop"));
  while (binFile.read(&block, 512) == 512)
  {
    if (block.count == 0)
    {
      break;
    }
    if (block.overrun)
    {
      if (!headerPrinted)
      {
        SER.println();
        SER.println(F("Overruns:"));
        SER.println(F("fileBlockNumber,sdBlockNumber,overrunCount"));
        headerPrinted = true;
      }
      SER.print(bn);
      SER.print(',');
      SER.print(binFile.firstBlock() + bn);
      SER.print(',');
      SER.println(block.overrun);
    }
    bn++;
  }
  if (!headerPrinted)
  {
    SER.println(F("No errors found"));
  }
  else
  {
    SER.println(F("Done"));
  }
}
//-----------------------------------------------------------------------------
// Convert binary file to csv file.
void binaryToCsv()
{
  uint8_t lastPct = 0;
  block_t block;
  uint32_t t0 = millis();
  uint32_t syncCluster = 0;
  SdFile csvFile;
  char csvName[FILE_NAME_DIM];

  if (!binFile.isOpen())
  {
    SER.println();
    SER.println(F("No current binary file"));
    return;
  }
  SER.println();
  SER.print(F("FreeStack: "));
  SER.println(FreeStack());

  // Create a new csvFile.
  strcpy(csvName, binName);
  int periodIndex = findChar(csvName, '.');
  strcpy(&csvName[periodIndex + 1], "csv");

  if (!csvFile.open(csvName, O_WRITE | O_CREAT | O_TRUNC))
  {
    error("open csvFile failed");
  }
  binFile.rewind();
  SER.print(F("Writing: "));
  SER.print(csvName);
  SER.println(F("\n - type any character to stop"));
  printHeader(&csvFile);
  char debMSG[30];
  uint32_t tPct = millis();

  clearSerial(&SER);
  while (!SER.available() && binFile.read(&block, 512) == 512) //
  {
    uint16_t i;
    if (block.count == 0 || block.count > DATA_DIM)
    {
      break;
    }
    if (block.overrun)
    {
      csvFile.print(F("OVERRUN,"));
      csvFile.println(block.overrun);
    }
    for (i = 0; i < block.count; i++)
    {
      printData(&csvFile, &block.data[i]);
    }
    if (csvFile.curCluster() != syncCluster)
    {
      csvFile.sync();
      syncCluster = csvFile.curCluster();
    }
    if ((millis() - tPct) > 1000)
    {
      uint8_t pct = binFile.curPosition() / (binFile.fileSize() / 100);
      if (pct != lastPct)
      {
        tPct = millis();
        lastPct = pct;
        SER.print(pct, DEC);
        SER.println('%');
      }
    }
    if (SER.available())
    {
      break;
    }
  } // while
  if (binFile.close())
    SER.print(F("Bin file closed. "));
  if (csvFile.close())
  {
    SER.print(F("CSV file Done: "));
    SER.print(0.001 * (millis() - t0));
    SER.println(F(" seconds"));
  }
}
//-----------------------------------------------------------------------------
// Send 512 byte blocks over serial
void binaryBlockDump()
{
  //Given a fileChoice, provide the user with '5000' all files, '6000' last file, or
  // '0-999' for specific files. The user will have to know the logic between
  //notifying the program when it's ready to receive data and when it'd like more.
  //~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**~-~**
  //DUE is currently set with TX/RX serial buffer size of 128 bytes.
  //Therefore need to use pointer to obtain data then point to
  //128 byte increments through the data transmission.What is the best way to do this?
  // Variables for tracking read binary files
  uint32_t fileSize;
  uint16_t msgSize;
  uint16_t arraySize = 100;
  uint16_t fileIndex = 0;
  uint16_t bytesSent = 0;
  char fileName[arraySize];
  fileName[0] = 0;
  char msg[arraySize];
  int availFiles;
  //Await serial input on which file to open
  while (!SER.available())
  {
    SysCall::yield();
  }
  uint32_t user_select = verifyNum(&SER, &DEB); //SER.parseInt();

  if (user_select >= 65525)
  {
    binFile.close();
    return;
    DEB.println("Didn't receive number..ending!");
  }
  //Clear out serial
  while (SER.available() && SER.read() > 0)
  {
  };
  DEB.print("Received # : ");
  DEB.print(user_select);
  DEB.println();

  //fileChoice: 6000 last file, 0-999 known file, or 4000 user select thru Serial
  //Look through avaiable files

  fileIndex = user_select;
  DEB.println("attempting to open");
  availFiles = openBinFileChoice(fileIndex, false);
  if (availFiles < 0)
  {
    DEB.println("Could not open file! WHy?!");
  }
  DEB.print("Tried opening, availFiles = ");
  DEB.println(availFiles);
  //Retrieve opened file and send over serial
  if (binFile.isOpen())
  {
    fileSize = binFile.fileSize();
    binFile.getName(fileName, arraySize);
    msgSize = sprintf(msg, "<m/%u><%s>", fileSize, fileName);
  }
  else
  {
    msgSize = sprintf(msg, "\n\n<m/0><Cannot open file at index %u>\n", fileIndex);
    return;
  }
  //    SER.write(msg, msgSize);
  DEB.println("Wrote msg");
  clearSerial(&SER);
  bytesSent = SER.write(msg, msgSize);
  if (bytesSent > 0)
  {
    DEB.write(msg, msgSize);
    DEB.println();
  }
  else
  {
    DEB.println("No msg sent!");
    SER.print("XX");
    return;
  }
  DEB.println("Awaiting fileSize");
  //Wait until a reply is received with the expected fileSize
  unsigned long timeOut = millis();
  while (!verifyReply(&SER, &DEB, fileSize))
  {
    if (millis() - timeOut > 2000)
    {
      SER.println("XX");
      DEB.println("Couldn't confirm reply!");
      return;
    }
  }

  DEB.println("Done verifyreply");

  // if(binFile.close()){ //Use for debugging so you don't go through file
  //   DEB.println("closed bin file. Return to main menu");
  //   return;
  // }
  //Setup variables to look through file
  block_t block;
  block_t *pblock = &block;
  //Assign a uint8_t pointer to sift bit by bit of the variable
  uint8_t *pbin_block = (uint8_t *)pblock;
  //    pbin_block = (uint8_t*)pblock;

  binFile.rewind();
  unsigned long t_serial, t_verply, t_loop;
  while (binFile.read(pblock, 512) == 512)
  {
    // t_loop = millis();
    //    uint16_t madeCount = (*(pbin_block+1) << 8) | *(pbin_block);  //Success!
    //wait for signal from esp8266 to send data
    t_serial = millis();
    while (!SER.available())
    {
      if (millis() - t_serial > 5000)
      {
        //likely some sort of error
        SER.println("<E><RxNoSerial>");
        binFile.close();
        return;
      }
    }

    //Read input data, no need for it now
    while (SER.available())
    {
      SER.read();
    }
    // t_serial = millis() - t_serial;
    // t_verply = millis();
    // DEB.write(SER.read());
    // DEB.print(millis()-t_delay);
    // DEB.println();

    for (uint16_t i = 0; i < 4; i++)
    { //Send 512 byte block in 4 - 128 byte packets
      bytesSent = SER.write(pbin_block + (i * 128), 128);
      verifyReply(&SER, &DEB, bytesSent);
    } //end for loop
    // t_verply = millis() - t_verply;
    // t_loop = millis() - t_loop;
    // char t_msg[30];
    // int t_size = sprintf(t_msg, "t_s: %u t_vr: %u t_l: %u\r\n",
    //                      t_serial, t_verply, t_loop);
    // DEB.write(t_msg, t_size);
    // DEB.println()
  } //end while loop of successful bin.read is successful
  clearSerial(&SER);
  binFile.close();
  delay(500); //no good

} //end of binaryBlockDump
//-----------------------------------------------------------------------------
void createBinFile()
{
  // max number of blocks to erase per erase call
  const uint32_t ERASE_SIZE = 262144L;
  uint32_t bgnBlock, endBlock;

  // Delete old tmp file.
  if (sd.exists(TMP_FILE_NAME))
  {
    SER.println(F("Deleting tmp file " TMP_FILE_NAME));
    if (!sd.remove(TMP_FILE_NAME))
    {
      error("Can't remove tmp file");
    }
  }
  // Create new file.
  SER.print(F("\nCreating new file..."));
  binFile.close();
  if (!binFile.createContiguous(TMP_FILE_NAME, 512 * FILE_BLOCK_COUNT))
  {
    error("createContiguous failed");
  }
  // Get the address of the file on the SD.
  if (!binFile.contiguousRange(&bgnBlock, &endBlock))
  {
    error("contiguousRange failed");
  }
  // Flash erase all data in the file.
  SER.print(F("Erasing all data... "));
  uint32_t bgnErase = bgnBlock;
  uint32_t endErase;
  while (bgnErase < endBlock)
  {
    endErase = bgnErase + ERASE_SIZE;
    if (endErase > endBlock)
    {
      endErase = endBlock;
    }
    if (!sd.card()->erase(bgnErase, endErase))
    {
      error("erase failed");
    }
    bgnErase = endErase + 1;
  }
}
//------------------------------------------------------------------------------
// dump data file to Serial
void dumpData()
{
  block_t block;
  if (!binFile.isOpen())
  {
    SER.println();
    SER.println(F("No current binary file"));
    return;
  }
  binFile.rewind();
  SER.println();
  SER.println(F("Type any character to stop"));
  delay(1000);
  printHeader(&SER);
  while (!SER.available() && binFile.read(&block, 512) == 512)
  {
    if (block.count == 0)
    {
      break;
    }
    if (block.overrun)
    {
      SER.print(F("OVERRUN,"));
      SER.println(block.overrun);
    }
    for (uint16_t i = 0; i < block.count; i++)
    {
      printData(&SER, &block.data[i]);
    }
  }
  if (binFile.close())
    SER.println(F("Data dump and file close success!"));
}
//------------------------------------------------------------------------------
// log data
void logData()
{

  createBinFile();
  recordBinFile();
  renameBinFile();
}
//------------------------------------------------------------------------------
void ls_Files(Stream *pr)
{
  SdBaseFile dirFile;
  // Number of files found.
  uint16_t n = 0;
  // Max of 1k files for the time being
  const uint16_t nMax = 999;
  // Position of file's directory entry.
  uint16_t dirIndex[nMax];
  uint16_t arrSize = 100;
  uint16_t msgMax = 128;
  uint16_t msgSize = 0;
  uint32_t fileSize = 0;
  int user_select;

  //confirm open files are closed
  if (binFile.isOpen())
    binFile.close();
  // List files in root directory.
  if (!dirFile.open("/", O_READ))
  {
    sd.errorHalt("open root failed");
  }

  pr->print(F("\nID |\tSize[Bytes] |\tName\n"));

  while (n < nMax && binFile.openNext(&dirFile, O_READ))
  {

    // Skip directories and hidden files.
    if (!binFile.isSubDir() && !binFile.isHidden())
    {

      // Save dirIndex of file in directory.
      dirIndex[n] = binFile.dirIndex();

      // Print the file number, size, and name
      char tempName[arrSize];
      char msg[msgMax];
      if (binFile.getName(tempName, arrSize))
      {
        //Retrieved name
        fileSize = binFile.fileSize();
        msgSize = sprintf(msg, "%u |\t%u |\t%s\n", n, fileSize, tempName);
        pr->write(msg, msgSize);
        delay(10);
      } //if can getName
      n++;
    } //if skip directories and hidden files
    binFile.close();
  } //while looking through Dir
  dirFile.close();

} //end void ls_Files
int openBinFileChoice(uint16_t fileChoice, bool echoSerial)
{
  //OpenBinFile of user choice:
  //fileChoice: 6000 last file in Dir, 0-999 known file, or 4000 user selects through Serial
  //echoSerial so suppress Serial output or not
  //return number of available files n OR -1 if there's an issue with opening a file
  //**Should do some error checking to confirm the requested file exists!
  SdBaseFile dirFile;
  // Number of files found.
  uint16_t n = 0;
  // Max of 1k files for the time being
  const uint16_t nMax = 999;
  uint16_t arrSize = 100;
  uint16_t msgMax = 128;
  uint32_t fileSize = 0;
  // Position of file's directory entry.
  uint16_t dirIndex[nMax];
  int user_select;

  //confirm open files are closed
  if (binFile.isOpen())
    binFile.close();
  // List files in root directory.
  if (!dirFile.open("/", O_READ))
  {
    sd.errorHalt("open root failed");
  }

  if (echoSerial)
    SER.print(F("\nID |\tSize[Bytes] |\tName\n"));

  while (n < nMax && binFile.openNext(&dirFile, O_READ))
  {
    // Skip directories and hidden files.
    if (!binFile.isSubDir() && !binFile.isHidden())
    {
      // Save dirIndex of file in directory.
      dirIndex[n] = binFile.dirIndex();
      // Print the file number, size, and name
      if (echoSerial)
      {
        char tempName[arrSize];
        char msg[msgMax];
        if (binFile.getName(tempName, arrSize))
        {
          //Retrieved name
          fileSize = binFile.fileSize();
          int msgSize = sprintf(msg, "%u |\t%u |\t%s\n", n, fileSize, tempName);
          SER.write(msg, msgSize);
          delay(10);
        } //if can getName
      }   //if echoSerial
      n++;
    }
    binFile.close();
  } //while looking through Dir

  // Read any existing Serial data.
  if (fileChoice == 4000)
  { //if User wants to select over serial
    do
    {
      delay(10);
    } while (SER.available() && SER.read() >= 0);
    if (echoSerial)
      SER.print(F("\nEnter File ID: "));

    while (!SER.available())
    {
      SysCall::yield();
    }
    user_select = SER.parseInt();
    if (echoSerial)
      SER.println(user_select);
  } //if User wants to select over serial
  else if (fileChoice == 6000)
  { //User wants the last file
    user_select = n - 1;
  }
  else
  { //User provides which index
    user_select = fileChoice;
  }
  //Confirm that user_select is within range
  if (user_select < 0 || user_select > n)
  {
    SER.println("Invalid fileChoice");
    return -2;
  }
  if (!binFile.open(&dirFile, dirIndex[user_select], O_READ))
  {
    SER.print("SD ERROR: index( ");
    SER.print(user_select);
    SER.print(") dirIndex: ");
    SER.print(dirIndex[user_select]);
    SER.write('\t');
    sd.errorHalt(F(" open"));
    return -1;
  }

  char name[FILE_NAME_DIM];
  if (binFile.getName(name, FILE_NAME_DIM))
  {
    if (echoSerial)
      SER.print(name);
    strcpy(binName, name);
  }
  else
  {
    if (echoSerial)
      SER.println(F("\r\nCannot get name of file\r\n"));
  }

  if (!sd.exists(name))
  {
    if (echoSerial)
      SER.println(F("File does not exist"));
    return -2;
  }
  binFile.close();
  dirFile.close();
  strcpy(binName, name);
  if (!binFile.open(binName, O_READ))
  {
    SER.println(F("open failed"));
    return -1;
  }
  if (echoSerial)
    SER.println(F(" File opened"));
  return n;
} //openBinFileChoice
//------------------------------------------------------------------------------
void openBinFile()
{
  SdBaseFile dirFile;
  // Number of files found.
  uint16_t n = 0;
  // Max of 1k files for the time being
  const uint16_t nMax = 999;
  // Position of file's directory entry.
  uint16_t dirIndex[nMax];
  int user_select;

  //confirm open files are closed
  if (binFile.isOpen())
    binFile.close();
  // List files in root directory.
  if (!dirFile.open("/", O_READ))
  {
    sd.errorHalt("open root failed");
  }
  SER.println();
  SER.print(F("\r\nID\tSize[Bytes]\tName\r\n"));
  while (n < nMax && binFile.openNext(&dirFile, O_READ))
  {

    // Skip directories and hidden files.
    if (!binFile.isSubDir() && !binFile.isHidden())
    {

      // Save dirIndex of file in directory.
      dirIndex[n] = binFile.dirIndex();

      // Print the file number, size, and name
      SER.print(n++);
      SER.write('\t');
      binFile.printFileSize(&SER);
      SER.write('\t');
      binFile.printName(&SER);

      SER.println();
    }
    binFile.close();
  } //while looking through Dir

  // Read any existing Serial data.
  do
  {
    delay(10);
  } while (SER.available() && SER.read() >= 0);
  SER.print(F("\r\nEnter File ID: "));

  while (!SER.available())
  {
    SysCall::yield();
  }
  user_select = SER.parseInt();
  SER.print(user_select);
  SER.write(' ');
  if (!binFile.open(&dirFile, dirIndex[user_select], O_READ))
  {
    sd.errorHalt(F(" open"));
  }

  char name[FILE_NAME_DIM];
  if (binFile.getName(name, FILE_NAME_DIM))
  {
    SER.print(name);
    strcpy(binName, name);
  }
  else
  {
    SER.println(F("\r\nCannot open indicated file\r\n"));
  }

  if (!sd.exists(name))
  {
    SER.println(F("File does not exist"));
    return;
  }
  binFile.close();
  dirFile.close();
  strcpy(binName, name);
  if (!binFile.open(binName, O_READ))
  {
    SER.println(F("open failed"));
    return;
  }
  SER.println(F(" File opened"));
}
//------------------------------------------------------------------------------
void recordBinFile()
{

  const uint8_t QUEUE_DIM = BUFFER_BLOCK_COUNT + 1;
  // Index of last queue location.
  const uint8_t QUEUE_LAST = QUEUE_DIM - 1;

  // Allocate extra buffer space.
  block_t block[BUFFER_BLOCK_COUNT - 1];

  block_t *curBlock = 0;

  block_t *emptyStack[BUFFER_BLOCK_COUNT];
  uint8_t emptyTop;
  uint8_t minTop;

  block_t *fullQueue[QUEUE_DIM];
  uint8_t fullHead = 0;
  uint8_t fullTail = 0;

  // Use SdFat's internal buffer.
  emptyStack[0] = (block_t *)sd.vol()->cacheClear();
  if (emptyStack[0] == 0)
  {
    error("cacheClear failed");
  }
  // Put rest of buffers on the empty stack.
  for (int i = 1; i < BUFFER_BLOCK_COUNT; i++)
  {
    emptyStack[i] = &block[i - 1];
  }
  emptyTop = BUFFER_BLOCK_COUNT;
  minTop = BUFFER_BLOCK_COUNT;

  // Start a multiple block write.
  if (!sd.card()->writeStart(binFile.firstBlock()))
  {
    error("writeStart failed");
  }
  SER.print(F("FreeStack: "));
  SER.println(FreeStack());
  int arraySize = 100;
  char headerConfig[arraySize];
  // DEB.println("Attempting to receive header...");
  int headerLength = receive_header(headerConfig, arraySize, &DEB);
  char header_ = headerConfig[1];
  if (headerLength > 1)
  {
    SER.print(F("Config | "));
    SER.write(&headerConfig[1], headerLength - 1);
    SER.println();
  }
  else
  {
    SER.print("Did not receive header");
    binFile.close();
    return;
  }
  //  SER.println(
  SER.println(F("Logging - type any character to stop"));
  bool closeFile = false;
  uint32_t bn = 0;
  uint32_t maxLatency = 0;
  uint32_t overrun = 0;
  uint32_t overrunTotal = 0;
  //  uint32_t logTime = micros();
  //Initialize mblc and encoders for run
  clearSerial(&Serial2);
  while (!Serial2.available())
  {
    Serial2.write('s');
  }
  delay(5);       //Necessary?...
  zeroEncoders(); //&pivotEnc, &pitchEnc);
  unsigned long t_print = millis();
  while (1)
  {

    if (SER.available())
    {
      SER.read();
      DEB.println("Ending MBLC");
      Serial2.write('e');

      // while (Serial2.available())
      // {
      //   Serial2.write('e');
      //   DEB.println("4 clearSerial");
      //   clearSerial(&Serial2);
      //   DEB.println("aft clearSerial");
      //   // delay(20);
      // }
      closeFile = true;
      DEB.println("closeFile true!");
    }
    if (millis() - t_print > 1000)
    {
      DEB.write('.');
      SER.println(".");
      t_print = millis();
    }
    if (closeFile)
    {
      if (curBlock != 0)
      {
        // Put buffer in full queue.
        fullQueue[fullHead] = curBlock;
        fullHead = fullHead < QUEUE_LAST ? fullHead + 1 : 0;
        curBlock = 0;
      } //if curBlock !=0
    }   //if closeFile
    else
    { //Do not closeFile
      if (curBlock == 0 && emptyTop != 0)
      {
        curBlock = emptyStack[--emptyTop];
        if (emptyTop < minTop)
        {
          minTop = emptyTop;
        }
        curBlock->count = 0;
        curBlock->overrun = overrun;
        overrun = 0;
      }
      //      if ((int32_t)(logTime - micros()) < 0) {
      //        error("Rate too fast");
      //      }
      //      int32_t delta;
      //      do {
      //        delta = micros() - logTime;
      //      } while (delta < 0);
      while (!Serial2.available())
      {
        SysCall::yield();
      }

      if (curBlock == 0)
      {
        overrun++;
        overrunTotal++;
        if (ERROR_LED_PIN >= 0)
        {
          digitalWrite(ERROR_LED_PIN, HIGH);
        }
#if ABORT_ON_OVERRUN
        SER.println(F("Overrun abort"));
        break;
#endif // ABORT_ON_OVERRUN
      }
      else
      {
#if USE_SHARED_SPI
        sd.card()->spiStop();
#endif // USE_SHARED_SPI
        acquireData(&curBlock->data[curBlock->count++]);
//        SER.write(".");
#if USE_SHARED_SPI
        sd.card()->spiStart();
#endif // USE_SHARED_SPI
        if (curBlock->count == DATA_DIM)
        {
          //          SER.print("cnt==dim, ");
          fullQueue[fullHead] = curBlock;
          fullHead = fullHead < QUEUE_LAST ? fullHead + 1 : 0;
          //          SER.println(fullHead);
          curBlock = 0;
        } //count == DATA_DIM
      }
    } //else do not closeFile

    if (fullHead == fullTail)
    {
      // Exit loop if done.
      if (closeFile)
      {
        break;
      }
    }
    else if (!sd.card()->isBusy())
    {
      // Get address of block to write.
      block_t *pBlock = fullQueue[fullTail];
      fullTail = fullTail < QUEUE_LAST ? fullTail + 1 : 0;

      // Write block to SD.
      uint32_t usec = micros();
      if (!sd.card()->writeData((uint8_t *)pBlock))
      {
        error("write data failed");
      }
      usec = micros() - usec;
      if (usec > maxLatency)
      {
        maxLatency = usec;
      }
      // Move block to empty queue.
      emptyStack[emptyTop++] = pBlock;
      bn++;
      if (bn == FILE_BLOCK_COUNT)
      {
        // File full so stop
        break;
      }
    }
  } //while {1}
  if (!sd.card()->writeStop())
  {
    error("writeStop failed");
  }
  SER.print(F("Min Free buffers: "));
  SER.println(minTop);
  SER.print(F("Max block write usec: "));
  SER.println(maxLatency);
  SER.print(F("Overruns: "));
  SER.println(overrunTotal);
  // Truncate file if recording stopped early.
  if (bn != FILE_BLOCK_COUNT)
  {
    SER.print(F("Truncating file #: "));
    SER.println(512L * bn);
    if (!binFile.truncate(512L * bn))
    {
      error("Can't truncate file");
    }
  }
}
//------------------------------------------------------------------------------
void recoverTmpFile()
{
  uint16_t count;
  if (!binFile.open(TMP_FILE_NAME, O_RDWR))
  {
    return;
  }
  if (binFile.read(&count, 2) != 2 || count != DATA_DIM)
  {
    error("Please delete existing " TMP_FILE_NAME);
  }
  SER.println(F("\nRecovering data in tmp file " TMP_FILE_NAME));
  uint32_t bgnBlock = 0;
  uint32_t endBlock = binFile.fileSize() / 512 - 1;
  // find last used block.
  while (bgnBlock < endBlock)
  {
    uint32_t midBlock = (bgnBlock + endBlock + 1) / 2;
    binFile.seekSet(512 * midBlock);
    if (binFile.read(&count, 2) != 2)
      error("read");
    if (count == 0 || count > DATA_DIM)
    {
      endBlock = midBlock - 1;
    }
    else
    {
      bgnBlock = midBlock;
    }
  }
  // truncate after last used block.
  if (!binFile.truncate(512 * (bgnBlock + 1)))
  {
    error("Truncate " TMP_FILE_NAME " failed");
  }
  renameBinFile();
}
//-----------------------------------------------------------------------------
void renameBinFile()
{
  //check if header exists
  int runCount = 0;
  int nameSize = 100;
  int longSize;
  int shortSize;
  char longHeader[nameSize];
  char shortHeader[nameSize];

  longSize = receive_header(longHeader, nameSize, &DEB);

  if (longSize > 0)
  {

    shortSize = reduceLabel(longHeader, shortHeader, longSize, &DEB);
    // shortHeader[shortSize] = 0;

    if (shortSize > 0)
    {
      memcpy(binName, shortHeader, shortSize);
      binName[shortSize] = (char)0;

      runCount = shortSize - 6;
    } //if short header exists
    else
    {
      runCount = BASE_NAME_SIZE;
    }
  } //if long header exists

  while (sd.exists(binName))
  {
    //      SER.println(binName);
    if (binName[runCount + 1] != '9')
    {
      binName[runCount + 1]++;
    }
    else
    {
      binName[runCount + 1] = '0';
      if (binName[runCount] == '9')
      {
        SER.println(binName);
        error("Can't create file name");
      }
      binName[runCount]++;
    }
  } //end while

  if (!binFile.rename(sd.vwd(), binName))
  {
    error("Can't rename file");
  }
  SER.print(F("File saved as: "));
  SER.println(binName);
  SER.print(F("File size: "));
  SER.print(binFile.fileSize() / 1024); //512);
  SER.println(F(" KB"));                //blocks"));
}
//------------------------------------------------------------------------------
void testSensor()
{
  //Works talking to separate MBLC
  data_t data;
  // SER.end();
  
  // Wait for Serial Idle.
  delay(500);
  //Print header
  int nameSize = 100;
  int longSize;
  char longHeader[nameSize];

  DEB.print("Config | ");
  

  longSize = receive_header(longHeader, nameSize, &DEB);
  if (longSize > 0)
  {
    DEB.write(&longHeader[1], longSize - 1);
    DEB.println();
    SER.print("Config | ");
    SER.write(&longHeader[1], longSize - 1);
    SER.println();
  }
  // return;
  // DEB.println("Ignoring return?");

  uint32_t lastTime = millis();
  clearSerial(&Serial2);
  Serial2.write('s');
  delay(5);
  zeroEncoders();
  printHeader(&DEB);
  //Repeat header to SER (what outputs in WiFi GUI)
  SER.println();
  // printHeader(&SER); //printing out bogus header, need to get header from mblc
  SER.println(F("\nTesting - type any character to stop\n"));

  while (!SER.available())
  {

    while (!Serial2.available())
    {
      SysCall::yield();
    }
    // acquireData(&data);
    if (acquireData(&data) && (data.time > lastTime))
    {
      printData(&DEB, &data);
      lastTime = data.time;
    } //if time has changed, print value
  }   // while !SER.avialable
  Serial2.write('e');
  clearSerial(&Serial2);
  clearSerial(&SER);
  clearSerial(&DEB);
  // SER.begin(115200);
} //end testSensor

void deleteFile()
{
  //Does not work!
  int choice = openBinFileChoice(4000, true);
  if (choice < 0 || choice > 6000)
  {
    SER.println("XX");
    binFile.close();
    return;
  } // try opening file

  SER.print(" ... attempting to delete.");

  bool successDEL = binFile.remove();
  if (successDEL)
  {
    SER.println(F(" successful removal!"));
    if (printDEB)
      DEB.println(F(" successful removal!"));
  } //if successDEL
  else
  {
    binFile.close();
    SER.println(F(" unsuccessful removal, manually delete!"));
    if (printDEB)
      DEB.println(F(" unsuccessful removal, manually delete!"));
  }

} //deleteFile

void wipeData()
{

  int c;

  SER.println(F("\r\nType 'Y' to wipe all data."));
  unsigned long t = millis();
  while (!SER.available())
  {
    SysCall::yield();
    if (millis() - t > 30000)
    {
      //Need input in 60 sec.
      SER.println(F("Received no input, quitting wipe."));
      return;
    }
  }
  c = SER.read();
  if (c != 'Y')
  {
    SER.println(F("Quitting, you did not type 'Y'."));
    return;
  }

  // Use wipe() for no dot progress indicator.
  if (!sd.wipe(&SER)) //MATLAB UDP not formatted to receive one char .
  {
    sd.errorHalt("Wipe failed.");
  }
  // Must reinitialize after wipe.
  // Initialize at the highest speed supported by the board that is
  // not over 50 MHz. Try a lower speed if SPI errors occur.
  if (!sd.begin(SD_CS_PIN, SPI_DIV3_SPEED))
  { //SD_SCK_MHZ(25))) {
    sd.initErrorPrint(&SER);
    fatalBlink();
  }
  SER.println("Done");

} // void wipeData

//------------------------------------------------------------------------------
void setup(void)
{
  if (ERROR_LED_PIN >= 0)
  {
    pinMode(ERROR_LED_PIN, OUTPUT);
  }

  SER.begin(115200);
  DEB.begin(115200);

  // Wait for USB Serial
  while (!SER)
  {
    SysCall::yield();
  }
  ignoreSerial(&SER, 1000);
  SER.print(F("\nFreeStack: "));
  SER.print(FreeStack());
  SER.print(F(" Records/block: "));
  SER.println(DATA_DIM);
  if (sizeof(block_t) != 512)
  {
    error("Invalid block size");
  }
  // Allow userSetup access to SPI bus.
  pinMode(SD_CS_PIN, OUTPUT);
  digitalWrite(SD_CS_PIN, HIGH);

  // Setup sensors.
  userSetup();

  // Initialize at the highest speed supported by the board that is
  // not over 50 MHz. Try a lower speed if SPI errors occur.
  if (!sd.begin(SD_CS_PIN, SPI_DIV3_SPEED))
  { //SD_SCK_MHZ(25))) {
    sd.initErrorPrint(&SER);
    fatalBlink();
  }
  // recover existing tmp file.
  if (sd.exists(TMP_FILE_NAME))
  {
    SER.println(F("\nType 'Y' to recover existing tmp file " TMP_FILE_NAME));
    if (printDEB)
      DEB.println(F("\nType 'Y' to recover existing tmp file " TMP_FILE_NAME));
    while (!SER.available())
    {
      SysCall::yield();
    }
    if (SER.read() == 'Y')
    {
      recoverTmpFile();
    }
    else
    {
      SER.print(F("'Y' not typed, deleting ... " TMP_FILE_NAME));
      if (printDEB)
        DEB.print(F("'Y' not typed, deleting ... " TMP_FILE_NAME));
      bool successDEL = sd.remove(TMP_FILE_NAME);
      if (successDEL)
      {
        SER.println(F(" successful removal!"));
        if (printDEB)
          DEB.println(F(" successful removal!"));
      }
      else
      {
        SER.println(F(" unsuccessful removal, manually delete!"));
        if (printDEB)
          DEB.println(F(" unsuccessful removal, manually delete!"));
      }
    }
  }
}
//------------------------------------------------------------------------------
void loop(void)
{
  // Read any Serial data.
  do
  {
    delay(100);
  } while (SER.available() && SER.read() >= 0);
  SER.println();
  // DEB.println(F("type:"));
  // DEB.println(F("~ - binary data dump"));
  // DEB.println(F("b - open existing bin file"));
  // DEB.println(F("c - convert file to csv"));
  // DEB.println(F("d - delete file of choice")); //dump data to Serial"));
  SER.println(F("||| e - overrun error details __"));
  SER.println(F("||| t - test over USB   |"));
  SER.println(F("||| w - wipe data                    |"));
  SER.println(F("||| r - record data                  |"));
  SER.println(F("||| l - list files                         |"));
  SER.println(F("type: ___________________"));
  while (!SER.available())
  {
    SysCall::yield();
  }
#if WDT_YIELD_TIME_MICROS
  SER.println(F("LowLatencyLogger can not run with watchdog timer"));
  SysCall::halt();
#endif

  char c = tolower(SER.read());

  // Discard extra Serial data.
  do
  {
    delay(10);
  } while (SER.available() && SER.read() >= 0);

  if (ERROR_LED_PIN >= 0)
  {
    digitalWrite(ERROR_LED_PIN, LOW);
  }
  switch (c)
  {
  case '~':
    binaryBlockDump();
    break;
  case 'b':
  {
    SER.println("Choosing to openBinFile with a choice...");
    int availFiles = openBinFileChoice(4000, true); //User choses if at 4000
    // SER.println();
    if (binFile.close())
      SER.println("Closed binFile");
    break;
  }
  case 'c':
  {
    int availFiles = openBinFileChoice(4000, true); //User choses if at 4000
    if (availFiles > 0)
      binaryToCsv();
    break;
  }
  case 'd':
    // dumpData();
    deleteFile();
    break;
  case 'e':
    checkOverrun();
    break;
  case 'i':
    ignoreSerial(&SER, 2000);
    break;
  case 'l':
    //      SER.println(F("\nls:"));
    ls_Files(&SER);
    ls_Files(&DEB);
    //      sd.ls(&SER, LS_SIZE);
    break;
  case 'r':
    logData();
    break;
  case 't':
    testSensor();
    break;
  case 'w':
    wipeData();
    break;
  default:
    SER.println(F("Invalid entry"));
  } //end switch
}
