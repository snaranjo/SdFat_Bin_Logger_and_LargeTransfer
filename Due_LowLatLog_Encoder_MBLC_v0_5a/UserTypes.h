#ifndef UserTypes_h
#define UserTypes_h
#include "Arduino.h"
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>


// User data types.  Modify for your data items.
#define FILE_BASE_NAME "BOMDAT"//"longFILEnamemblc6dat"
const uint8_t MBLC_DIM = 6; //num of output variables from MBLC 
const uint8_t ENC_DIM = 2;  //num of encoders

struct data_t {
  uint32_t time;
  float encoders[ENC_DIM];
  float mblc[MBLC_DIM];
};

//Fun Definitions

float getDegrees( Encoder *encoder, long CPR);
void zeroEncoders (); //Encoder *encoder1, Encoder * encoder2);
bool acquireData(data_t* data);
void printData(Print* pr, data_t* data);
void printHeader(Print* pr);
void clearSerial( Stream * pr); 
int readSerialbreak( Stream * sp, Stream * db, char * charArr, char breakChar);
int reduceLabel( char origArr[], char newArr[], int arrSize, Stream * pr);
int receive_header(char rxChars[], int charArrLen, Stream * pr);
int findChar ( char charArr[], char starChar);
bool verifyReply (Stream* pr, Stream* db, uint32_t sentMSGsize);
void ignoreSerial( Stream * pr, long delayTime);
uint32_t verifyNum(Stream *pr, Stream *db);

void userSetup();
#endif  // UserTypes_h
