#include "UserTypes.h"
// User data functions.  Modify these functions for your data items.

//Encoder Settings
extern Encoder pivotEnc(2, 3);
extern Encoder pitchEnc(4, 5);
//   avoid using pins with LEDs attached
const long encoderCPR = 40000;
const float pivotRatio = encoderCPR * (72 / 24);  //72-24
const float pitchRatio = encoderCPR * (128 / 16); //128-16

// Sensor setup
void userSetup()
{
  Serial2.begin(115200); //MBLC interface
  // Serial3.begin(115200);  //Debug interace
  //     Serial3.begin(115200); //WIFI interface
}

// Start time for data
static uint32_t startMicros;
//uint32_t t_ref;

float getDegrees(Encoder *encoder, long CPR)
{
  return ((float)encoder->read() / CPR) * 360;
} //get Degrees

void zeroEncoders()
{ //(Encoder * encoder1, Encoder * encoder2) {

  pivotEnc.write(0);
  pitchEnc.write(0);
} //void zeroEncoders

void random_digits(float inArray[], int sizeArr)
{
  //Given a blank (or not) float arry, create random
  //numbers to populate them. The should work for a
  //sizeArr sized Array

  for (int i = 0; i < sizeArr; i++)
  {
    inArray[i] = 0.0; //clear out element
    //At this point I'm going to replicate ranges similar
    //to minitaur outputs
    long randVal = 0.0;
    switch (i)
    {
    case 0:
      // statement
      randVal = random(0, 9000);
      break;
    case 1:
      // statements
      randVal = random(13500, 27000);
      break;
    default:
      // statements
      randVal = random(-17999, 17999);
    } //end switch-case

    inArray[i] = (float)randVal / 100.00;
  } //end for loop
} //end random_digits

// Acquire a data record.
bool acquireData(data_t *data)
{
  // Read all channels to avoid SD write latency between readings.
  //  random_digits(data->mblc, MBLC_DIM );
  int num_rxChars = 0;
  int byteBuffer = 4*MBLC_DIM;
  while (Serial2.available())
  {
    num_rxChars = Serial2.readBytes((uint8_t *)data->mblc, byteBuffer);
    if (num_rxChars == byteBuffer)
    {
      data->time = millis();
      data->encoders[0] = getDegrees(&pivotEnc, pivotRatio); //pivotDeg
      data->encoders[1] = getDegrees(&pitchEnc, pitchRatio); //pitchDeg
      // random_digits(data->encoders, ENC_DIM); //simulate retreving encoder data
    }
  }
  if (num_rxChars == byteBuffer)
  {
    return true;
  }
  else
    return false;
} //acquireData

// Print a data record.
void printData(Print *pr, data_t *data)
{
  if (startMicros == 0)
  {
    startMicros = data->time;
  }
  pr->print(data->time - startMicros);
  pr->write(',');
  pr->print(data->encoders[0]);
  pr->write(',');
  pr->print(data->encoders[1]);
  for (int i = 0; i < MBLC_DIM; i++)
  {
    pr->write(',');
    pr->print(data->mblc[i]);
  }
  pr->println();
}

// Print data header.
void printHeader(Print *pr)
{
  startMicros = 0;
  pr->print(F("time(ms), pivot(deg), pitch (deg)"));

  for (int i = 0; i < MBLC_DIM; i++)
  {
    pr->print(F(",mblc "));
    pr->print(i);
  }
  pr->println();
}

void clearSerial(Stream *pr)
{
  while (pr->available() > 0)
  {
    pr->read();
  }
} //void clearSerial

int findChar(char charArr[], char starChar)
{
  //Simple function to look for a ctrl character
  //and assume that's the end of the char arry
  int i = 0;
  while (charArr[i] != starChar)
  {
    i++;
  }
  return i; //i-1 number of characters in string
} // int findCtrlChar

int reduceLabel(char origArr[], char newArr[], int arrSize, Stream *pr)
{
  //funciton to eliminate all nonAlphaNumeric numbers and
  //make label compact for file name.
  //also need to combine non-interest chars
  // return size of newArr

  int j = 0; //counter for newArr
  int i = 0; //counter for origArr
  int decision = 0;
  bool prevNonAlphaNum = false;
  bool endFlag = false;

  while (!endFlag)
  { //parse through each char

    bool isAlphaNum = isAlphaNumeric(origArr[i]);

    if (isAlphaNum)
    {
      decision = 1;
      prevNonAlphaNum = false;
    }

    else
    { //not Alphanumeric

      int intChar = (int)origArr[i];
      if (intChar == 45)
      { // is it - , keep as is
        decision = 1;
        prevNonAlphaNum = false;
      }
      else if (intChar == 46)
      { // is it . convert to _
        decision = 2;
        prevNonAlphaNum = true;
      } //if a period

      else if ((intChar == 60) || (intChar == 62))
      { // do nothing if <>
        decision = 0;
        prevNonAlphaNum = true;
      } //is it  < >

      else
      { // do we add - or nothing?

        if (prevNonAlphaNum)
        {
          if (isControl(origArr[i]))
          {
            decision = 4;
          }
          else
          {
            decision = 0;
          }
        }
        else
        { //prev is AlphaNumeric
          if (isDigit(origArr[i - 1]))
          {
            decision = 5;
          }
          else
          {
            decision = 3;
            prevNonAlphaNum = true;
          } //isNotNumeric
        }   // apply - or nothing
      }     //else convert to underscore?
    }       //end else not alphanumeric

    //1 copyChar 2 applyUnderscore 3 applyDash 4 control char
    if (decision == 1)
    {
      newArr[j++] = origArr[i];
    }
    else if (decision == 2)
    {
      newArr[j++] = '_';
    }
    else if (decision == 3)
    {
      newArr[j++] = '#';
    }
    else if (decision == 4)
    { //assume first ctrl char is end of string
      newArr[++j] = 0;
      endFlag = true;
    } //end decision logic

    if (i < arrSize)
    {
      i++;
    }
    else
    {
      endFlag = true;
    }

  } //end while loop
  //Add the .bin to end of file
  newArr[j] = 0;
  // pr->print("Orig: ");
  // pr->println(origArr);
  // pr->print("B4 ext: ");
  // pr->println(newArr);
  char fileExt[] = "!00.bin";
  for (int i = 0; i < 7; i++)
  {
    newArr[j++] = fileExt[i];
  }
  newArr[j] = 0;
  // pr->print("Ext: ");
  // pr->println(newArr);

  //  // Apply .ext
  // strcpy(csvName, binName);
  // int periodIndex = findChar(csvName, '.');
  // strcpy(&csvName[periodIndex + 1], "csv");

  return j;
} // void reduceLabel

int receive_header(char rxChars[], int charArrLen, Stream *pr)
{
  int numbytes_rx;
  int rxSize = 0;
  int cnt = 0;
  // pr->println("rx_hdr FUN:");
  clearSerial(&Serial2);
  Serial2.write('h');
  unsigned long t_delay = millis();
  while (!Serial2.available())
  {
    if (millis() - t_delay > 20)
    {
      cnt++;
      t_delay = millis();
      if (cnt >= 2)
      {
        pr->println("Timeout for header");
        return -1;
      }
      Serial2.write('h');
    }
  };
  numbytes_rx = Serial2.readBytes((uint8_t *)rxChars, charArrLen);

  if (numbytes_rx > 0)
  {
    // pr->print(cnt);
    // pr->write(' ');
    // pr->println(millis() - t_delay);
    rxSize = findChar(rxChars, '>');
    //clear out any other garbage received
    for (int i = rxSize + 1; i < charArrLen; i++)
    {
      rxChars[i] = (char)0;
    }
  }
  return rxSize; //reducedSize;
                 // // pr->println("Ready to read serial...");
                 // //receiving these bytes takes approx. 1 sec. for teensyMBLC_sim
                 // // numbytes_rx = Serial2.readBytes((uint8_t *) rxChars, charArrLen);
                 // numbytes_rx = readSerialbreak(&Serial2, pr, rxChars, '>');
                 // clearSerial(&Serial2);
                 // rxChars[numbytes_rx-1] = 0;

  // return (numbytes_rx-1); //rxSize;

} //void receive_header

int collectSerialPacket(Stream *sp, Stream *db, char *charArr)
{
  // sp->print("collSP ");
  int i = 0;
  unsigned long t;

  // while(!sp->available());
  t = millis();

  while (millis() - t < 10)
  {
    if (sp->available())
    {
      charArr[i++] = (char)sp->read();
      db->write(charArr[i - 1]);
      db->write(' ');
      t = millis();
      if (charArr[i - 1] == '>')
        break;
    }
  } //end while
  return i;
}

int readSerialbreak(Stream *sp, Stream *db, char *charArr, char breakChar)
{
  int i = 0;
  while (!sp->available())
    ;
  unsigned long t_del = millis();
  while (sp->available() || millis() - t_del < 5)
  {
    if (sp->available())
    {
      char c = sp->read();
      charArr[i++] = c;
      // db->write(c);
      // db->write(' ');
      // db->write('(');
      // db->print(millis()-t_del);
      // db->write(')');
      // db->write(' ');
      t_del = millis();
      if (c == breakChar)
        break;
    }
  }
  // db->println();

  return i;
}
bool verifyReply(Stream *pr, Stream *db, uint32_t sentMSGsize)
{

  int rxMSGsize = 0;
  uint16_t rxArrSize = 15;
  char rxBuffer[rxArrSize];
  bool loopflag = true;

  uint16_t i = readSerialbreak(pr, db, rxBuffer, '>');

  if (!i)
    return false;
  //await correspondence of received message
  // while (!pr->available()) //SysCall::yield();;
  // i = collectSerialPacket(pr, db, rxBuffer);
  // unsigned long t = millis();
  // //Read incoming message
  //   while (millis()-t < 5) {
  //     if (pr->available() )    rxBuffer[i++] = pr->read();
  //   //Found end of message
  //   if (rxBuffer[i-1] == '>')  break;
  //   }
  rxBuffer[i] = 0;
  // clearSerial(pr);

  //Look through msg to find size w/ format <NUM>
  int endChar = findChar(rxBuffer, '>');
  if (rxBuffer[endChar] == '>' && rxBuffer[0] == '<')
  {
    int j = 0;
    char *msg_ptr = &rxBuffer[i - 1];
    while (*--msg_ptr != '<')
    {
      rxMSGsize += (*msg_ptr - '0') * pow(10, j++);
    }
    // db->write('\n');
    // db->print(rxMSGsize);
    // db->write('\t');
    // db->print(rxBuffer);
    // db->write('\t');
    // db->println(sentMSGsize);
    return (rxMSGsize == sentMSGsize);
  } //indicate that we found a properly formatted msg
  else
  {
    db->print("Could not find '<' & '>' | ");
    db->println(rxBuffer);
    return false;
  }
} // void verifyReply
uint32_t verifyNum(Stream *pr, Stream *db)
{
  uint32_t rxNum = 0;
  uint16_t rxArrSize = 15;
  char rxBuffer[rxArrSize];
  bool nonNum = false;
  uint16_t i = readSerialbreak(pr, db, rxBuffer, '>');

  if (!i)
    return 65545;

  rxBuffer[i] = 0;

  //Look through msg to find size w/ format <NUM>
  int endChar = findChar(rxBuffer, '>');
  if (rxBuffer[endChar] == '>' && rxBuffer[0] == '<')
  {
    int j = 0;
    char *msg_ptr = &rxBuffer[i - 1];
    while (*--msg_ptr != '<')
    {
      if (*msg_ptr > 47 && *msg_ptr < 58) //between ASCII 0-9
        rxNum += (*msg_ptr - '0') * pow(10, j++);
      else
        nonNum = true;
    }
    if (!nonNum)
      return rxNum;
    else
      return 65565;
  } //indicate that we found a properly formatted msg
  else
  {
    db->print("Could not find '<' & '>' | ");
    db->println(rxBuffer);
    return false;
  }
} // void verifyReply
void ignoreSerial(Stream *pr, long delayTime)
{
  //ESP8266 spits garbage over serial everytime it resets.
  //Therefore read in all the garbage until delayTime is met and
  long t = millis();
  while (pr->available() || (millis() - t) < delayTime)
  {
    pr->read();
  } //end while loop
} //void ignoreSerial
