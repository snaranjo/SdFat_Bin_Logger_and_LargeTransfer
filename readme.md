Standalone data logger program for the DUE to collect local data from a timer and two encoders.
It communicates over serial to the Minitaur MBLC board that is sending serial data at a regular interval. SdFat library is used for logging all data into a .bin file.
It communicates over a serial line to the ESP8266 (wifi dongle) to review SD data, control data collection and perform file transfers over WiFi. 

The data collection frequency is controlled by the rate at which the MBLC data comes in via Serial, set to 200 Hz. The Due expects MBLC data to send a total of 6 float variables. If that number is to change, it needs to be reflected here, the MBLC code, and the matlab GUI. 

In order to reprogram, need to include the SdFat.h library. 

This program was compiled with SdFat.h version 26 Apr 2017 and Arduino IDE 1.8.6.  